addEventListener('load', function () {
    Calendar.init({
        disablePastDays: false
    });
})

let Calendar = {
    month: document.querySelectorAll('[data-calendar-area="month"]')[0],
    next: document.querySelectorAll('[data-calendar-toggle="next"]')[0],
    previous: document.querySelectorAll('[data-calendar-toggle="previous"]')[0],
    label: document.querySelectorAll('[data-calendar-label="month"]')[0],
    activeDates: null,
    date: new Date,
    todaysDate: new Date,
    init: function (t) {
        this.options = t, this.date.setDate(1), this.createMonth(), this.createListeners()
    },
    createListeners: function () {
        let t = this;
        this.next.addEventListener("click", function () {
            t.clearCalendar();
            let e = t.date.getMonth() + 1;
            t.date.setMonth(e), t.createMonth()
        }), this.previous.addEventListener("click", function () {
            t.clearCalendar();
            let e = t.date.getMonth() - 1;
            t.date.setMonth(e), t.createMonth()
        })
    },
    createDay: function (t, e) {
        let n = document.createElement("div"), s = document.createElement("span");
        s.innerHTML = t, n.className = "cal-date", n.setAttribute("data-calendar-date", this.date), 1 === t && (n.style.marginLeft = 0 === e ? 6 * 14.28 + "%" : 14.28 * (e - 1) + "%"), this.date.getTime() <= this.todaysDate.getTime() - 1 ? n.classList.add() : (n.classList.add("cal-date-active"), n.setAttribute("data-calendar-status", "active")), this.date.toString() === this.todaysDate.toString() && n.classList.add("cal-date-today"), n.appendChild(s), this.month.appendChild(n)
        s.innerHTML = t, n.className = "cal-date", n.setAttribute("data-calendar-date", this.date), 1 === t && (n.style.marginLeft = 0 === e ? 6 * 14.28 + "%" : 14.28 * (e - 1) + "%"), this.options.disablePastDays && this.date.getTime() <= this.todaysDate.getTime() - 1 ? n.classList.add() : (n.classList.add("cal-date-active"), n.setAttribute("data-calendar-status", "active")), this.date.toString() === this.todaysDate.toString() && n.classList.add("cal-date-today"), n.appendChild(s), this.month.appendChild(n)

    },

    dateClicked: function () {
        let t = this;
        this.activeDates = document.querySelectorAll('[data-calendar-status="active"]');
        for (let e = 0; e < this.activeDates.length; e++) this.activeDates[e].addEventListener("click", function (e) {
            document.querySelectorAll('[data-calendar-label="picked"]')[0].innerHTML = this.dataset.calendarDate, t.removeActiveClass(),
                this.classList.add("cal-date-selected")
        })
    },
    createMonth: function () {
        for (let t = this.date.getMonth(); this.date.getMonth() === t;) this.createDay(this.date.getDate(), this.date.getDay(), this.date.getFullYear()), this.date.setDate(this.date.getDate() + 1);
        this.date.setDate(1), this.date.setMonth(this.date.getMonth() - 1), this.label.innerHTML = this.monthsAsString(this.date.getMonth()) + " " + this.date.getFullYear(), this.dateClicked()
    },
    monthsAsString: function (t) {
        return ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"][t]
    },

    clearCalendar: function () {

        Calendar.month.innerHTML = ""
    },

    removeActiveClass: function () {
        for (let t = 0; t < this.activeDates.length; t++) this.activeDates[t].classList.remove("cal-date-selected")
    }

};

